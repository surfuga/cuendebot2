# -*- coding: utf-8 -*-


import os, sys
import getpass
import json
import requests
import re
import random
import yaml

from flask import Flask, request, jsonify, Response
app = Flask(__name__)

conf = yaml.load(open(os.path.abspath(__file__).replace('.py','.conf'),'r'))
botkey = os.environ.get('BOTK',"")
botsecret = os.environ.get('BOTS',"")
thepassword = os.environ.get('PASSWORD')

def updateStats(dataDict):
    try:
        with open(os.path.abspath(__file__).replace('.py','.stats'),'r+') as f:
            prev = yaml.load(f)
            id = dataDict["inline_query"]["id"]
            name = dataDict["inline_query"]["name"]
            username = dataDict["inline_query"]["username"]
            query = dataDict["inline_query"]["query"]
            if id in prev.keys():
                prev[id][count]+=1
                prev[id][last]=query
            else:
                prev[id] = { 'name': name, 'username': username, count: 1, last: query}
            f.close()
            f=open(os.path.abspath(__file__).replace('.py','.stats'),'w+')
            yaml.dump(prev, f)
    except:
        pass

def printStats():
    try:
        with open(os.path.abspath(__file__).replace('.py','.stats'),'r+') as f:
            prev = yaml.load(f)
            return prev
    except:
        return {id:'0','name':'nothing'}
        	

@app.route("/", methods=['GET','POST'])
def hello():

    global conf, botkey, botsecret

    dataDict = json.loads(request.data)
    if 'inline_query' not in dataDict.keys():
        return ' ', 200

    updateStats(dataDict)
    thearray2 = conf["responses"]

    # if query is not '' ->
    selected = []
    query = None

    if 'inline_query' in dataDict.keys() and 'query' in dataDict['inline_query'].keys():
        query = dataDict["inline_query"]["query"].lower()
        #print("query:")
        #print(query.encode('utf-8'))

    #list option
    #if query == '*':
    #    selected = thearray2

    # search option
    result_type = None
    if query and len(query) > 0 :
        for voice in thearray2:
            if voice['title'].lower().find(query) != -1:
                selected.append(voice)
                if voice['type'] == 'voice':
                    result_type = 'voice'
        if result_type == 'voice':
            selected = [ elem for elem in selected if elem['type'] == 'voice' ]
        if query == thepassword:
            selected = printStats()

    if len(selected) == 0:
        i = random.randint(0,len(thearray2))
        selected.append(thearray2[i]) 
 
    response = {} 
    response['inline_query_id']= dataDict['inline_query']['id']
    response['query_id']= dataDict['inline_query']['id']
    response['results'] = json.dumps(selected)
 
    url = 'https://api.telegram.org/bot'+botkey+':'+botsecret+'/answerInlineQuery'
    data = json.dumps(response)
    r = requests.post(url,response)
    return ' ', 200


@app.after_request
def after(response):
    # todo with response
    #print(response.status)
    #print(response.headers)
    #print(response.get_data())
    return response


if __name__ == "__main__":
    port = int(os.environ.get('PORT',50000))
    app.run(host='0.0.0.0',port=port, debug=True)
